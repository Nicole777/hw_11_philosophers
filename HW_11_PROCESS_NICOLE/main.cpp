#include <cstdio>
#include <windows.h>
#include <iostream>


#define PHIL_AMOUNT 5
// chopstick declararion // 
bool is_chopstick_used0 = false;
bool is_chopstick_used1 = false;
bool is_chopstick_used2 = false;
bool is_chopstick_used3 = false;
bool is_chopstick_used4 = false;
int phNames[] = { 1, 2, 3, 4, 5 };

// critical section declaration //
CRITICAL_SECTION critical_section_chopstick0;
CRITICAL_SECTION critical_section_chopstick1;
CRITICAL_SECTION critical_section_chopstick2;
CRITICAL_SECTION critical_section_chopstick3;
CRITICAL_SECTION critical_section_chopstick4;
CRITICAL_SECTION outputs_critical_section;


DWORD WINAPI eatNoodles(LPVOID params); //thread's function

int main(int argc, char* argv[]) 
{
    HANDLE threads[PHIL_AMOUNT];


    //initialization of critical saction
    InitializeCriticalSection(&critical_section_chopstick0);
    InitializeCriticalSection(&critical_section_chopstick1);
    InitializeCriticalSection(&critical_section_chopstick2);
    InitializeCriticalSection(&critical_section_chopstick3);
    InitializeCriticalSection(&critical_section_chopstick4);
    InitializeCriticalSection(&outputs_critical_section);
    printf("The start of the dinner.\n");

    // creating 5 threads
    for (int i = 0; i < 5; ++i) 
    {
        threads[i] = CreateThread(nullptr, 0, eatNoodles, &phNames[i], 0, nullptr);
    }

    WaitForMultipleObjects(PHIL_AMOUNT, threads, true, 1000000);


    // removing critical section declaration
    DeleteCriticalSection(&critical_section_chopstick0);
    DeleteCriticalSection(&critical_section_chopstick1);
    DeleteCriticalSection(&critical_section_chopstick2);
    DeleteCriticalSection(&critical_section_chopstick3);
    DeleteCriticalSection(&critical_section_chopstick4);
    DeleteCriticalSection(&outputs_critical_section);

    for (int i = 0; i < PHIL_AMOUNT; i++) 
    {
        CloseHandle(&threads[i]);
    }

    return 0;
}

DWORD WINAPI eatNoodles(LPVOID params) 
{
    int* name_ptr = (int*)params;
    int philosopher = *name_ptr;

    while (true) 
    {
        if (philosopher == 1) {
            EnterCriticalSection(&critical_section_chopstick0);
            if (!is_chopstick_used0) {
                is_chopstick_used0 = true;
                LeaveCriticalSection(&critical_section_chopstick0);
                EnterCriticalSection(&critical_section_chopstick1);
                if (!is_chopstick_used1) {
                    is_chopstick_used1 = true;
                    EnterCriticalSection(&outputs_critical_section);
                    printf("Philosopher #%d is eating:\n", philosopher);
                    LeaveCriticalSection(&outputs_critical_section);
                    LeaveCriticalSection(&critical_section_chopstick1);
                    Sleep(1);
                    EnterCriticalSection(&critical_section_chopstick1);
                    is_chopstick_used1 = false;
                    LeaveCriticalSection(&critical_section_chopstick1);
                    EnterCriticalSection(&critical_section_chopstick0);
                    is_chopstick_used0 = false;
                    EnterCriticalSection(&outputs_critical_section);
                    printf("Philosopher #%d is thinking:\n", philosopher);
                    LeaveCriticalSection(&outputs_critical_section);
                    LeaveCriticalSection(&critical_section_chopstick0);

                    Sleep(1);
                }

                else 
                {
                    LeaveCriticalSection(&critical_section_chopstick1);
                    EnterCriticalSection(&critical_section_chopstick0);
                    is_chopstick_used0 = false;
                    LeaveCriticalSection(&critical_section_chopstick0);
                }
            }
            else 
            {
                LeaveCriticalSection(&critical_section_chopstick0);
            }
        }
        if (philosopher == 2) {
            EnterCriticalSection(&critical_section_chopstick1);
            if (!is_chopstick_used1) {
                is_chopstick_used1 = true;
                LeaveCriticalSection(&critical_section_chopstick1);
                EnterCriticalSection(&critical_section_chopstick2);
                if (!is_chopstick_used2) {
                    is_chopstick_used2 = true;
                    EnterCriticalSection(&outputs_critical_section);
                    printf("Philosopher #%d is eating:\n", philosopher);
                    LeaveCriticalSection(&outputs_critical_section);
                    LeaveCriticalSection(&critical_section_chopstick2);

                    sleep(1);
                    EnterCriticalSection(&critical_section_chopstick2);
                    is_chopstick_used2 = false;
                    LeaveCriticalSection(&critical_section_chopstick2);
                    EnterCriticalSection(&critical_section_chopstick1);
                    is_chopstick_used1 = false;
                    EnterCriticalSection(&outputs_critical_section);
                    printf("Philosopher #%d is thinking:\n", philosopher);
                    LeaveCriticalSection(&outputs_critical_section);
                    LeaveCriticalSection(&critical_section_chopstick1);

                    sleep(1);
                }
                else {
                    LeaveCriticalSection(&critical_section_chopstick2);
                    EnterCriticalSection(&critical_section_chopstick1);
                    is_chopstick_used1 = false;
                    LeaveCriticalSection(&critical_section_chopstick1);
                }
            }
            else {
                LeaveCriticalSection(&critical_section_chopstick1);
            }
        }
        if (philosopher == 3) {
            EnterCriticalSection(&critical_section_chopstick2);
            if (!is_chopstick_used2) {
                is_chopstick_used2 = true;
                LeaveCriticalSection(&critical_section_chopstick2);
                EnterCriticalSection(&critical_section_chopstick3);
                if (!is_chopstick_used3) {
                    is_chopstick_used3 = true;
                    EnterCriticalSection(&outputs_critical_section);
                    printf("Philosopher #%d is eating:\n", philosopher);
                    LeaveCriticalSection(&outputs_critical_section);
                    LeaveCriticalSection(&critical_section_chopstick3);

                    sleep(1);
                    EnterCriticalSection(&critical_section_chopstick3);
                    is_chopstick_used3 = false;
                    LeaveCriticalSection(&critical_section_chopstick3);
                    EnterCriticalSection(&critical_section_chopstick2);
                    is_chopstick_used2 = false;
                    EnterCriticalSection(&outputs_critical_section);
                    printf("Philosopher #%d is thinking:\n", philosopher);
                    LeaveCriticalSection(&outputs_critical_section);
                    LeaveCriticalSection(&critical_section_chopstick2);

                    sleep(1);
                }
                else {
                    LeaveCriticalSection(&critical_section_chopstick3);
                    EnterCriticalSection(&critical_section_chopstick2);
                    is_chopstick_used2 = false;
                    LeaveCriticalSection(&critical_section_chopstick2);
                }
            }
            else {
                LeaveCriticalSection(&critical_section_chopstick2);
            }
        }
        if (philosopher == 4) {
            EnterCriticalSection(&critical_section_chopstick3);
            if (!is_chopstick_used3) {
                is_chopstick_used3 = true;
                LeaveCriticalSection(&critical_section_chopstick3);
                EnterCriticalSection(&critical_section_chopstick4);
                if (!is_chopstick_used4) {
                    is_chopstick_used4 = true;
                    EnterCriticalSection(&outputs_critical_section);
                    printf("Philosopher #%d is eating:\n", philosopher);
                    LeaveCriticalSection(&outputs_critical_section);
                    LeaveCriticalSection(&critical_section_chopstick4);

                    sleep(1);
                    EnterCriticalSection(&critical_section_chopstick4);
                    is_chopstick_used4 = false;
                    LeaveCriticalSection(&critical_section_chopstick4);
                    EnterCriticalSection(&critical_section_chopstick3);
                    is_chopstick_used3 = false;
                    EnterCriticalSection(&outputs_critical_section);
                    printf("Philosopher #%d is thinking:\n", philosopher);
                    LeaveCriticalSection(&outputs_critical_section);
                    LeaveCriticalSection(&critical_section_chopstick3);

                    sleep(1);
                }
                else {
                    LeaveCriticalSection(&critical_section_chopstick4);
                    EnterCriticalSection(&critical_section_chopstick3);
                    is_chopstick_used3 = false;
                    LeaveCriticalSection(&critical_section_chopstick3);
                }
            }
            else {
                LeaveCriticalSection(&critical_section_chopstick3);
            }
        }
        if (philosopher == 5) {
            EnterCriticalSection(&critical_section_chopstick4);
            if (!is_chopstick_used4) {
                is_chopstick_used4 = true;
                LeaveCriticalSection(&critical_section_chopstick4);
                EnterCriticalSection(&critical_section_chopstick0);
                if (!is_chopstick_used0) {
                    is_chopstick_used0 = true;
                    EnterCriticalSection(&outputs_critical_section);
                    printf("Philosopher #%d is eating:\n", philosopher);
                    LeaveCriticalSection(&outputs_critical_section);
                    LeaveCriticalSection(&critical_section_chopstick0);

                    sleep(1);
                    EnterCriticalSection(&critical_section_chopstick0);
                    is_chopstick_used0 = false;
                    LeaveCriticalSection(&critical_section_chopstick0);
                    EnterCriticalSection(&critical_section_chopstick4);
                    is_chopstick_used4 = false;
                    EnterCriticalSection(&outputs_critical_section);
                    printf("Philosopher #%d is thinking:\n", philosopher);
                    LeaveCriticalSection(&outputs_critical_section);
                    LeaveCriticalSection(&critical_section_chopstick4);

                    sleep(1);
                }
                else {
                    LeaveCriticalSection(&critical_section_chopstick0);
                    EnterCriticalSection(&critical_section_chopstick4);
                    is_chopstick_used4 = false;
                    LeaveCriticalSection(&critical_section_chopstick4);
                }
            }
            else {
                LeaveCriticalSection(&critical_section_chopstick4);
            }
        }
    }
}